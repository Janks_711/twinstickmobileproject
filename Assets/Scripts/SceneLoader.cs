﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// This script will be used to handle Scene Transition
/// Will make this a singleton class and will keep it active across multiple scenes
/// </summary>

public class SceneLoader : MonoBehaviour
{
    [HideInInspector] public static SceneLoader instance { get; private set; }
    [SerializeField] GameObject fadeObject = null; // Get the Fade image with animation
    Animator fadeAnimation = null;
    Image fadeImage = null;

    // Need to subscribe to Scene Loaded for the fade out transition
    private void OnEnable()
    {
        SceneManager.sceneLoaded += FadeOut;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= FadeOut;
    }

    // Singleton class
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        fadeAnimation = fadeObject.GetComponent<Animator>();
        fadeImage = fadeObject.GetComponent<Image>();
    }

    // This handles the screen Fade in
    public IEnumerator FadeIn(int index)
    {
        GameManager.instance.PauseGame(false);
        fadeAnimation.SetBool("isTransitioning", true);
        yield return new WaitUntil(() => fadeImage.color.a == 1);
        SceneManager.LoadScene(index);
    }

    // This handles the screen fade out
    public void FadeOut(Scene scene, LoadSceneMode mode)
    {
        if (!fadeAnimation) { return; }
        fadeAnimation.SetBool("isTransitioning", false);

        StopAllCoroutines();
    }

    // Load the game scene
    public void LoadGameScene()
    {      
        StartCoroutine(FadeIn(1));
    }

    // Load the menu scene
    public void LoadMenuScene()
    {
        StartCoroutine(FadeIn(0));
    }
    

}
