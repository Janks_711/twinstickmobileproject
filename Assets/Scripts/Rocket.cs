﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script will determine the functionality of the player Projectile rocket
/// </summary>

public class Rocket : MonoBehaviour
{
    [Header("Rocket Settings")]
    [SerializeField] float acceleration = 1f;
    [SerializeField] float speed = 50f;
    [SerializeField] float timeToDestroy = 3f;
    [SerializeField] float damage = 100f;

    private void Start()
    {
        // Set the rocket to destroy after x seconds
        Destroy(gameObject, timeToDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        speed += acceleration * Time.deltaTime; // Accelerate over time
        transform.Translate(transform.up * speed * Time.deltaTime, Space.World);
    }

    // Damage any hit drones
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Drone>())
        {
            other.gameObject.GetComponent<Drone>().TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}
