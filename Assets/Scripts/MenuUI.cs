﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Controls the Main Menu interface
/// </summary>

public class MenuUI : MonoBehaviour
{
    SceneLoader sceneLoader = null;

    // Start is called before the first frame update
    void Start()
    {
        sceneLoader = SceneLoader.instance;
    }

    // Start the game
    public void StartGame()
    {
        sceneLoader.LoadGameScene();
    }
}
