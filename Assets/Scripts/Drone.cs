﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Drone Script, just a fairly simple Patrol Drone that destroys when shot
/// </summary>

public class Drone : MonoBehaviour
{
    [Header("Drone Settings")]
    [SerializeField] float health = 300f;
    [SerializeField] float speed = 2f;
    [SerializeField] float rotateSpeed = 20f;
    [SerializeField] [Range(0, 1)] float stopDistance = .5f;

    [Header("Patrol Settings")]
    [SerializeField] Transform[] patrolPath = null;
    int currentWaypoint = 0; // Current waypoint patrolling to

    [Header("Effects Settings")]
    [SerializeField] ParticleSystem explosion = null;
    [SerializeField] AudioClip hitClip = null;
    [SerializeField] AudioClip explosionClip = null;
    AudioSource audioSource = null;

    private void Start()
    {
        explosion = GetComponentInChildren<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Patrol();
    }

    // Patrol a set of waypoints
    // Always resets at first waypoint
    private void Patrol()
    {
        if(patrolPath.Length == 0) { return; } // Make sure the path is not empty

        // Patrol if this isn't the last waypoint on the path
        if (currentWaypoint != patrolPath.Length)
        {
            var direction = patrolPath[currentWaypoint].position - transform.position;
            var distance = direction.magnitude;

            // Movement will only occur on the X and Y axes, but the waypoint's will still need to be low to the ground for the distance condition
            if (distance > stopDistance)
            {
                var dir = direction.normalized;
                var noUpMovement = new Vector3(dir.x, 0, dir.z); // Make sure movement and rotation is only on x and z axes
                transform.forward = Vector3.Slerp(transform.forward, noUpMovement, rotateSpeed * Time.deltaTime);
                transform.position += noUpMovement * speed * Time.deltaTime;
            }
            else
            {
                currentWaypoint++;
            }
        }
        // Reset Path
        else
        {
            currentWaypoint = 0;
        }
    }

    // Take Damage
    public void TakeDamage(float value)
    {
        health -= value;

        if(health <= 0)
        {
            Death();
        }
        else
        {
            if(!hitClip) { return; }
            audioSource.PlayOneShot(hitClip);
        }
    }

    // Sad Deaths only
    private void Death()
    {
        GetComponentInChildren<Renderer>().enabled = false; // Disable the render straight after death
        GetComponent<Collider>().enabled = false; // Stops from colliding when the Drone is destoyed
        if(explosion != null)
        {
            if(!explosionClip) { return; }
            audioSource.PlayOneShot(explosionClip);
            explosion.Play();
        }
        Destroy(gameObject, explosionClip.length); // Destroy gameObject when particle system is finished
    }
}
