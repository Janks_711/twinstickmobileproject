﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// A script for the functions of the virtual joysticks
/// </summary>

public class Joystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [Header("Joystick Settings")]
    [SerializeField] Image backgroundImg = null;
    [SerializeField] Image joystickImg = null;
    [SerializeField][Range(0,1)] float deadZone = 0.2f;
    Vector3 inputVector = Vector3.zero;

    [SerializeField] [Range(2f, 10f)] float joystickMoveLimit = 2.0f; // How far the joystick can move beyond the background image
                                                                  // Clamped this, because going below 2 breaks the joystick

    // Start is called before the first frame update
    void Start()
    {
        backgroundImg = GetComponentInChildren<Image>();
        joystickImg = backgroundImg.transform.GetChild(0).GetComponent<Image>();
    }

    // Defines what happens when the user uses the Joystick
    public void OnDrag(PointerEventData data)
    {
        Vector2 touchPosition; // Determine postion of where the user touches the screen

        // Using the data provided in the Pointer Event data, determine if the user is touching the Joystick Background
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImg.rectTransform, data.position, data.pressEventCamera, out touchPosition))
        {
            touchPosition.x = (touchPosition.x / backgroundImg.rectTransform.sizeDelta.x);
            touchPosition.y = (touchPosition.y / backgroundImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(touchPosition.x * 2, 0, touchPosition.y * 2); // Multiply the touch position * 2, to increase the joystick movement.

            // DeadZone calculation, using the radial Deadzone method
            if(inputVector.magnitude < deadZone)
            {
                inputVector = Vector2.zero;
            }

            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            // Move the joystick image to where the user has pressed
            joystickImg.rectTransform.anchoredPosition = new Vector3(inputVector.x * (backgroundImg.rectTransform.sizeDelta.x / joystickMoveLimit),
                                                                    inputVector.z * (backgroundImg.rectTransform.sizeDelta.y / joystickMoveLimit));
        }
    }

    // When user presses on the screen, move the joystick
    public void OnPointerDown(PointerEventData data)
    {
        OnDrag(data);
    }

    // Reset joystick if user lifts their finger
    public void OnPointerUp(PointerEventData data)
    {
        inputVector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    // Return axis for Horizontal and Vertical for player movement
    public float Horizontal()
    {
        if (inputVector.x != 0)
        {
            return inputVector.x;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }

    public float Vertical()
    {
        if (inputVector.z != 0)
        {
            return inputVector.z;
        }
        else
        {
            return Input.GetAxis("Vertical");
        }
    }
}
