﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Will use this class to control the game Mute and switch mute Sprite
/// </summary>

public class MuteButton : MonoBehaviour
{
    [SerializeField] Sprite mutedSprite = null;
    [SerializeField] Sprite notMutedSprite = null;
    [SerializeField] Image img = null;

    private void Start()
    {
        img = GetComponent<Image>();
    }

    // Mute Audio Listener
    public void ToggleMute()
    {
        AudioListener.pause = !AudioListener.pause;

        // Switch Sprite if aavailable
        if(AudioListener.pause)
        {
            if(!mutedSprite) { return; }
            img.sprite = mutedSprite;
        }
        else
        {
            if(!notMutedSprite) { return; }
            img.sprite = notMutedSprite;
        }
    }
}
