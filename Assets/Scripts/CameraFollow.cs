﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This will allow the main camera to follow the Player
/// </summary>

public class CameraFollow : MonoBehaviour
{ 
    [Header("Camera Settings")]
    [SerializeField] Transform player = null;
    [SerializeField] float speed = 1f;
    Vector3 boom = Vector3.zero;
    
    // Start is called before the first frame update
    void Start()
    {
        // Get the current Vector from the target to the Camera
        boom = transform.position - player.position;
    }

    // Update is called once per frame
    void Update()
    {
        // Set our position to be the same relative to the player
        Vector3 playerPos = player.position + boom;

        transform.position = Vector3.Lerp(transform.position, playerPos, speed * Time.deltaTime);
    }
}
