﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines the function for the pause button
/// </summary>

public class PauseButton : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu = null;
    bool isPaused = false;

    private void Start()
    {
        if (!pauseMenu) { return; }
        pauseMenu.SetActive(false);
        isPaused = false;
    }

    public void TogglePause()
    {
        if(!pauseMenu) { return; }

        isPaused = !isPaused;

        if(isPaused)
        {
            GameManager.instance.PauseGame(true);
            pauseMenu.SetActive(true);
        }
        else
        {
            GameManager.instance.PauseGame(false);
            pauseMenu.SetActive(false);
        }
    }
}
