﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script for handling the UI functions in the Game menu
/// </summary>

public class GameUIScript : MonoBehaviour
{
    [Header("Menu Objects")]
    [SerializeField] GameObject pauseMenu = null;
    [SerializeField] GameObject wonMenu = null;
    [SerializeField] TMPro.TextMeshProUGUI dronesLeftText = null;
    GameManager gameManager;
    SceneLoader sceneLoader;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.instance;
        sceneLoader = SceneLoader.instance;
        pauseMenu.SetActive(false);
        wonMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        DronesRemaingText();

        if(gameManager.IsGameEnded())
        {
            if(!wonMenu) { return; }
            if(wonMenu.activeInHierarchy == false)
            {
                wonMenu.SetActive(true);
            }
        }
    }

    // Used for the pause button
    public void PauseButton()
    {
        if (!pauseMenu) { return; }

        if (gameManager.IsGamePaused())
        {
            gameManager.PauseGame(false);
            pauseMenu.SetActive(false);
        }
        else
        {
            gameManager.PauseGame(true);
            pauseMenu.SetActive(true);
        }
    }

    // Resume Game
    public void ResumeGame()
    {
        if (!pauseMenu) { return; }

        if (gameManager.IsGamePaused())
        {
            gameManager.PauseGame(false);
        }

        if (pauseMenu.activeInHierarchy)
        {
            pauseMenu.SetActive(false);
        }
    }

    // Reset game function
    public void ResetGame()
    {
        sceneLoader.LoadGameScene();
    }

    // Quit Game Function
    public void QuitGame()
    {
        sceneLoader.LoadMenuScene();
    }

    // Display how many drones remaining
    public void DronesRemaingText()
    {
        if (dronesLeftText != null)
        {
            dronesLeftText.text = "Drones Remaining: " + gameManager.EnemiesRemaining();
        }

    }
}
