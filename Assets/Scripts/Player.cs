﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script that defines the basic Player functions
/// </summary>

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    CharacterController playerController = null;
    [Header("Player Movement")]
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float rotateSpeed = 5f;
    [SerializeField] Joystick joyLeft = null;
    [SerializeField] Joystick joyRight = null;
    bool rightStickEngaged = false; // Determine if the player is currently aiming with the right stick so we can fire when this is true.
    Quaternion lastTorsoRotation = Quaternion.identity; // Use this to make sure player doesn't reset the rotation when stopped
    float speedBeforeBreaking = 0f; // To calculate deacceleration in Move function

    [Header("Robot Wheels")]
    [SerializeField] GameObject leftWheel = null;
    [SerializeField] GameObject rightWheel = null;
    [SerializeField] float wheelRotation = 10f;

    [Header("Torso Rotation")]
    [SerializeField] GameObject torso = null;
    [SerializeField] float turrentRotationSpeed = 5f;

    [Header("Shooting")]
    [SerializeField] GameObject projectile = null;
    [SerializeField] Transform[] firePoints = null;
    [SerializeField][Range(0,1)] float fireRate = 1.5f;
    float fireTimer = 0.0f;
    int currentFirePoint = 0; // Which gun to fire out

    [Header("Audio")]
    [SerializeField] AudioClip idleAudioClip = null;
    [SerializeField] AudioClip movingAudioClip = null;
    [SerializeField] AudioClip shootAudioClip = null;
    AudioSource audioSource = null;

    // Start is called before the first frame update
    void Start()
    {
        playerController = GetComponent<CharacterController>();
        audioSource = GetComponent <AudioSource>();
        audioSource.clip = idleAudioClip;

        torso = transform.GetChild(1).gameObject; // Will grab the Torso gameobject (Provided that it remains 2nd in the Bot prefab)
        fireTimer = fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        RotateTorso();

        // Timer for the fire Rate
        fireTimer -= 1.0f * Time.deltaTime;

        if(rightStickEngaged && fireTimer < 0.0f)
        {
            Fire();
            audioSource.PlayOneShot(shootAudioClip);
        }
    }

    // Defines the shooting function for the player
    private void Fire()
    {
        if (!projectile)
        {
            Debug.Log(name + " doesn't have a Projectile Assigned!");
        }

        if (firePoints.Length < 2)
        {
            Debug.Log("One or more firePoints are not assigned to the " + name);
        }

        int currFirePoint = (currentFirePoint % 2 == 0) ? 0 : 1;
        Instantiate(projectile, firePoints[currFirePoint].position, firePoints[currFirePoint].transform.rotation);
        currentFirePoint++;
        fireTimer = fireRate;
    }

    // Roates the torso when the user has turn the right stick
    private void RotateTorso()
    {
        if (!joyRight)
        {
            Debug.Log("Right Joystick Not Assigned!");
            return;
        }

        //Get movement Axes from the right Joystick
        var horizontalRotation = joyRight.Horizontal();
        var verticalRotation = joyRight.Vertical();

        //Set Rotation to be that of the Right joystick
        Vector3 rotationDir = new Vector3(horizontalRotation, 0, verticalRotation);
        torso.transform.right = Vector3.Lerp(torso.transform.right, rotationDir, turrentRotationSpeed * Time.deltaTime);

        // Reset the rotation if no longer using the right stick
        // Also if there is movement, fire the weapon
        if (horizontalRotation == 0 && verticalRotation == 0)
        {
            torso.transform.localRotation = Quaternion.Slerp(torso.transform.localRotation, Quaternion.Euler(Vector3.zero), turrentRotationSpeed * Time.deltaTime);
            rightStickEngaged = false;
        }
        else
        {
            rightStickEngaged = true;
        }
    }

    // Move the player
    private void Move()
    {
        if(!joyLeft)
        {
            Debug.Log("Left Joy stick not defined");
            return;
        }

        // Get movement axes from Left Joystick
        var horizontalMovement = joyLeft.Horizontal();
        var verticalMovement = joyLeft.Vertical();

        // Set the rotation to be of the left joystick
        Vector3 rotationDir = new Vector3(horizontalMovement, 0, verticalMovement);

        var currSpeed = Vector3.Magnitude(rotationDir) * moveSpeed;

        // Breaking
        if (horizontalMovement == 0 && verticalMovement == 0)
        {
            // This code just adds a little bit of deacceleration before stopping
            if(speedBeforeBreaking > 0)
            {
                playerController.SimpleMove(transform.right * speedBeforeBreaking * Time.deltaTime);
                speedBeforeBreaking -= moveSpeed * Time.deltaTime;            
            }
            transform.rotation = lastTorsoRotation;
            SwitchAudio(idleAudioClip);
        }
        // Moving
        else
        {
            transform.right = Vector3.Lerp(transform.right, rotationDir, rotateSpeed * Time.deltaTime);
            lastTorsoRotation = transform.rotation;

            // If there is input on either axis, move the player forward
            if (horizontalMovement != 0 || verticalMovement != 0)
            {
                playerController.SimpleMove(transform.right * currSpeed * Time.deltaTime);
                speedBeforeBreaking = currSpeed;
                SwitchAudio(movingAudioClip);
            }
        }
        RotateWheels(currSpeed);
    }

    // Rotate the wheels as the player moves or turns
    private void RotateWheels(float currSpeed)
    {

        var verticalMovement = joyLeft.Vertical();
        var horizontalMovement = joyLeft.Horizontal();

        // Rotation if the player is moving forward
        if (leftWheel != null && rightWheel != null)
        {
            // Get wheel height
            var wheelSpeed = currSpeed * wheelRotation;

            if (verticalMovement != 0)
            {
                leftWheel.transform.Rotate(Vector3.up * wheelSpeed * Time.deltaTime);
                rightWheel.transform.Rotate(Vector3.up * -wheelSpeed * Time.deltaTime);
            }
        }
    }

    // This function will switch the Audio clip if needed
    private void SwitchAudio(AudioClip clip)
    {
        if (audioSource.clip != clip)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
            audioSource.clip = clip;
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }
}
