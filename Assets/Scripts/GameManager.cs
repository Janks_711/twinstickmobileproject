﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Game Manager to control the game loop functions, ie has the player won
/// This will be a singletonclass
/// </summary>

public class GameManager : MonoBehaviour
{
    [HideInInspector] public static GameManager instance { get; private set; }
    [SerializeField] Drone[] dronesInGame = null;
    bool isPaused = false;
    bool gameEnded = false;

    // Subscribe to scene loaded event to Reset the drone count
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelLoad;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelLoad;
    }

    // using the Awake function to make sure this is the only gamemanager in the game
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "GameScene" && EnemiesRemaining() <= 0)
        {
            EndGame();
        }
    }

    // What happens when the game ends
    private void EndGame()
    {
        gameEnded = true;
    }

    //Toggles pause
    public void PauseGame(bool value)
    {
        if (value == true)
        {
            Time.timeScale = 0;
            isPaused = true;
        }
        else
        {
            Time.timeScale = 1;
            isPaused = false;
        }
    }

    // Track how many enemies remain
    public int EnemiesRemaining()
    {
        int count = dronesInGame.Length;

        foreach(Drone drone in dronesInGame)
        {
            if(drone == null)
            {
                count--;
            }
        }
        return count;
    }

    // Return the game's paused status
    public bool IsGamePaused()
    {
        return isPaused;
    }

    // has the game ended
    public bool IsGameEnded()
    {
        return gameEnded;
    }

    public void OnLevelLoad(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "GameScene")
        {
            ResetGame();
            Debug.Log("SceneLoaded: " + scene.name);
        }
       PauseGame(false);
    }

    private void ResetGame()
    {
        dronesInGame = FindObjectsOfType<Drone>(); // Get all Enemies, when all Destroyed the Player wins
        gameEnded = false;
    }
}
